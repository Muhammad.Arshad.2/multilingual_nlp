# -*- coding: utf-8 -*-
"""TP4.py

Developed in the notebook test.ipynb, where computations and plots are shown.

Authors:
    - Faysal Saber
    - Azeem Arshad
    - Petter Stahle
"""

import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

from transformers import AutoTokenizer, AutoModelForQuestionAnswering, AutoModelForSequenceClassification, TextClassificationPipeline, pipeline


MAX_TOKEN_LENGTH = 512

# open txt file from command line argument adn  save as dataframe
def open_txt_file(file_path):
    with open(file_path, "r",encoding= 'utf-8' ) as f:
        data = f.readlines()
    df = pd.DataFrame(data, columns=['text'])
    df = df.apply(lambda x: x.str.replace("\n",""))
    return df


if __name__ == '__main__':

    input_path = sys.argv[1]

    print('Loading language classifier model...')
    model_name = 'qanastek/51-languages-classifier'
    tokenizer = AutoTokenizer.from_pretrained(model_name)
    model = AutoModelForSequenceClassification.from_pretrained(model_name)
    classifier = TextClassificationPipeline(model=model, tokenizer=tokenizer, return_all_scores=False)


    df = open_txt_file(input_path)

    xquad_unique = ['ar' ,'de', 'el', 'en', 'es', 'hi', 'ru', 'ro']
    xcopa_unique = ['et','ht','id','it','qu','sw','ta']
    common = ['th', 'tr', 'vi' ,'zh']

    languages = xquad_unique + xcopa_unique + common
    languages.__len__()

    # add columns language and dataset to df
    df['language'] = ''
    df['dataset'] = ''

    for i in tqdm(range(len(df))): 
        
        text = df['text'][i]
        if len(text) > MAX_TOKEN_LENGTH:
            text = text[:MAX_TOKEN_LENGTH]

        res = classifier(text)[-1]['label'][:2]
        
        if res in xquad_unique:
            df['dataset'][i] = 'xquad'
            df['language'][i] = res
        elif res in xcopa_unique:
            df['dataset'][i] = 'xcopa'
            df['language'][i] = res
        elif res in common:
            df['dataset'][i] = 'common'
            df['language'][i] = res
        else:
            df['dataset'][i] = 'other'
            df['language'][i] = res

   
    """### Handle "other" category for missclassified languages"""
    #change classifier to return all scores 
    classifier = TextClassificationPipeline(model=model, tokenizer=tokenizer, return_all_scores=True)

    idx = np.where(df['dataset'] == 'other')[0]

    for i in tqdm(idx):
        text = df['text'][i]
        if len(text) > MAX_TOKEN_LENGTH:
            text = text[:MAX_TOKEN_LENGTH]
        res = classifier(text)[0]

        scores = pd.DataFrame(res).sort_values(by='score', ascending=False)
        scores['label'] = scores['label'].apply(lambda x : x[:2])
        language = scores[scores['label'].isin(languages)].head(1)['label'].values[0]

        if language == 'fi': language = 'et'
        if language == 'ms': language = 'id'
        if language == 'cy':
            if 'Mwen' in text: language = 'ht'
            else: language = 'qu'

        df['language'][i] = language
        
        if language in xquad_unique:
            df['dataset'][i] = 'xquad'
        elif language in xcopa_unique:
            df['dataset'][i] = 'xcopa'
        elif language in common:
            df['dataset'][i] = 'common'

    # How we reclassified the 'other' dataset

    # fi --> et
    # ms --> id
    # cy, sq --> qu, ht
    # ar --? reclassify (tr/ru )
    # jv --? reclassify (ht, sw, id)


    """#### Fix XQUAD missclassifications"""

    THRESHOLD = 15

    idx = np.where(df['dataset'] == 'xquad')[0]

    for i in tqdm(idx):
        if df['text'][i].split().__len__() < THRESHOLD:
            df['dataset'][i] = 'xcopa'


    """#### Reclassify common category"""

    idx = np.where(df['dataset'] == 'common')[0]

    for i in tqdm(idx):
        if df['text'][i].split().__len__() < THRESHOLD:
            df['dataset'][i] = 'xcopa'
        else:
            df['dataset'][i] = 'xquad'


    # Save
    df.to_csv('output_tp4.txt', index=False, sep='\t')
    print('Done!')

